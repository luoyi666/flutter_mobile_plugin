import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'page/index.dart';

void main() async {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: const HomePage(),
      builder: (context, child) => Material(
        child: child!,
      ),
    );
  }
}
