import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';

import '../global.dart';

class DemoTestPage extends StatefulWidget {
  const DemoTestPage(this.title, {super.key});

  final String title;

  @override
  State<DemoTestPage> createState() => _DemoTestPageState();
}

class _DemoTestPageState extends State<DemoTestPage> with SingleTickerProviderStateMixin<DemoTestPage> {
  late AnimationController controller;
  late Animation<double> animation;
  late Animation<double> radiusAnimation;
  int count = 0;
  double _currentSliderPrimaryValue = 0.2;
  final double _currentSliderSecondaryValue = 0.5;

  late GestureTapCallback throttleFun = AsyncUtil.throttle(() {
    setState(() {
      count++;
    });
  }, 1000);

  void listenAnimation() {
    if (mounted) {
      setState(() {});
    }
  }

  void listenAnimationStatus(AnimationStatus status) {
    if (status == AnimationStatus.completed) {
      controller.reverse();
    } else if (status == AnimationStatus.dismissed) {
      controller.forward();
    }
  }

  @override
  void initState() {
    super.initState();
    controller = AnimationController(vsync: this, duration: const Duration(milliseconds: 300));
    animation =
        Tween<double>(begin: 200, end: 300).animate(CurvedAnimation(parent: controller, curve: Curves.fastOutSlowIn));
    radiusAnimation =
        Tween<double>(begin: 4, end: 32).animate(CurvedAnimation(parent: controller, curve: Curves.fastOutSlowIn));
    animation.addListener(listenAnimation);
    animation.addStatusListener(listenAnimationStatus);
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SelectionArea(
      child: CupertinoPageScaffold(
        navigationBar: CupertinoNavigationBar(
          middle: Text(widget.title),
          previousPageTitle: '插件测试',
        ),
        child: SafeArea(
          child: Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                SliderTheme(
                  data: SliderThemeData(
                    inactiveTrackColor: Colors.grey.withOpacity(0.5),
                    thumbShape: const RoundSliderThumbShape(enabledThumbRadius: 6),
                    overlayShape: const RoundSliderOverlayShape(overlayRadius: 12),
                  ),
                  child: Slider(
                    value: _currentSliderPrimaryValue,
                    secondaryTrackValue: _currentSliderSecondaryValue,
                    label: _currentSliderPrimaryValue.round().toString(),
                    onChanged: (double value) {
                      setState(() {
                        _currentSliderPrimaryValue = value;
                      });
                    },
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    i('点击了容器');
                  },
                  child: Container(
                    width: animation.value,
                    height: animation.value,
                    decoration: BoxDecoration(
                      color: Colors.grey,
                      borderRadius: BorderRadius.circular(radiusAnimation.value),
                    ),
                  ),
                ),
                CupertinoButton(
                    child: const Text('启动动画'),
                    onPressed: () async {
                      controller.forward();
                    }),
                Material(
                  color: Colors.grey,
                  elevation: 2,
                  borderRadius: BorderRadius.circular(6),
                  child: InkWell(
                    onTap: throttleFun,
                    hoverColor: Colors.red,
                    borderRadius: BorderRadius.circular(6),
                    child: Container(
                      padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                      child: Text(
                        'count: $count',
                        style: const TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 8),
                ElevatedButton(
                  onPressed: throttleFun,
                  child: Text(
                    'count: $count',
                  ),
                ),
                const Text("Hello World!").animate().fadeIn(duration: 500.ms).scale().tint(color: Colors.purple),
                Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      width: 100,
                      height: 50,
                      color: Colors.grey.shade300,
                    ),
                    const SizedBox(width: 8),
                    Container(
                      width: 150,
                      height: 50,
                      color: Colors.grey.shade300,
                    ),
                    const SizedBox(width: 8),
                    Container(
                      width: 100,
                      height: 50,
                      color: Colors.grey.shade300,
                    )
                  ],
                ).animate(onPlay: (controller) => controller.repeat()).shimmer(duration: 1200.ms, color: Colors.grey),
                const Text("Hello World")
                    .animate(onPlay: (controller) => controller.repeat())
                    .shimmer(duration: 1200.ms, color: const Color(0xFF80DDFF))
                    .custom(
                        duration: 1200.ms,
                        builder: (context, value, child) {
                          return Container(
                            color: Color.lerp(Colors.red, Colors.blue, value),
                            padding: const EdgeInsets.all(8),
                            child: child, // child is the Text widget being animated
                          );
                        }),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
