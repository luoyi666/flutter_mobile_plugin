import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../global.dart';

class ClickTestPage extends StatefulWidget {
  const ClickTestPage(this.title, {super.key});

  final String title;

  @override
  State<ClickTestPage> createState() => _ClickTestPageState();
}

class _ClickTestPageState extends State<ClickTestPage> {
  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        middle: Text(widget.title),
        previousPageTitle: '插件测试',
      ),
      child: SafeArea(
        child: buildCenterColumn(
          [
            Material(
              color: Colors.grey,
              child: InkWell(
                onTap: () {
                  i('点击了父容器');
                },
                child: SizedBox(
                  width: 300,
                  height: 300,
                  child: AbsorbPointer(
                    absorbing: true,
                    child: Column(
                      children: [
                        GestureDetector(
                          onTap: () {
                            i('点击了子容器');
                          },
                          child: Container(
                            width: 100,
                            height: 100,
                            color: Colors.green,
                          ),
                        ),
                        ElevatedButton(
                          onPressed: () {
                            i('点击了按钮');
                          },
                          child: const Text('按钮'),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
            const SizedBox(height: 8),
            Material(
              color: Colors.grey,
              child: InkWell(
                onTap: () {
                  i('点击了父容器');
                },
                child: SizedBox(
                  width: 300,
                  height: 300,
                  child: IgnorePointer(
                    child: Column(
                      children: [
                        GestureDetector(
                          onTap: () {
                            i('点击了子容器');
                          },
                          child: Container(
                            width: 100,
                            height: 100,
                            color: Colors.green,
                          ),
                        ),
                        ElevatedButton(
                          onPressed: () {
                            i('点击了按钮');
                          },
                          child: const Text('按钮'),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                i('点击了最外部');
              },
              child: Stack(
                alignment: Alignment.center,
                children: [
                  GestureDetector(
                    onTap: () {
                      i('点击了红色方块');
                    },
                    onLongPress: () {
                      i('长按了红色方块');
                    },
                    child: Container(
                      width: 142,
                      height: 142,
                      color: Colors.red,
                    ),
                  ),
                  IgnorePointer(
                    child: ElevatedButton(
                      onPressed: () {
                        i('点击了按钮');
                      },
                      child: const Text('按钮'),
                    ),
                  ),
                  // GestureDetector(
                  //   onTap: () {
                  //     i('点击了蓝色方块');
                  //   },
                  //   child: Container(
                  //     width: 42,
                  //     height: 42,
                  //     color: Colors.blue,
                  //   ),
                  // ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class IgnoreListTestPage extends StatefulWidget {
  const IgnoreListTestPage({super.key});

  @override
  State<IgnoreListTestPage> createState() => _IgnoreListTestPageState();
}

class _IgnoreListTestPageState extends State<IgnoreListTestPage> {
  bool ignore = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Ignore点击事件测试'),
      ),
      body: IgnorePointer(
        ignoring: ignore,
        child: ListView.builder(
          itemCount: 100,
          itemBuilder: (context, index) => ListTile(
            onTap: () {},
            title: Text('列表- ${index + 1}'),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            ignore = !ignore;
          });
        },
        child: Icon(ignore ? Icons.lock_outline : Icons.lock_open),
      ),
    );
  }
}
