import 'package:flutter/cupertino.dart';

import '../global.dart';

class VibrateTestPage extends StatelessWidget {
  const VibrateTestPage(this.title, {super.key});

  final String title;

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        middle: Text(title),
        previousPageTitle: '插件测试',
      ),
      child: Center(
        child: CupertinoButton.filled(
          onPressed: () {
            // Vibration.vibrate();
            VibrateUtil.createVibrate();
          },
          child: const Text('触发震动'),
        ),
      ),
    );
  }
}
