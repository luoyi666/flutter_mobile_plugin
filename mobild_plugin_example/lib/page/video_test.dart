import 'package:flutter/cupertino.dart';

import '../global.dart';

class VideoTestPage extends StatefulWidget {
  const VideoTestPage(this.title, {super.key});

  final String title;

  @override
  State<VideoTestPage> createState() => _VideoTestPageState();
}

class _VideoTestPageState extends State<VideoTestPage> {
  bool fill = false;

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        middle: Text(widget.title),
        previousPageTitle: '插件测试',
      ),
      child: SafeArea(
        child: VideoWidget(
          url: 'https://media.w3.org/2010/05/sintel/trailer.mp4',
          fill: fill,
          autoPlay: false,
        ),
        // child: Stack(
        //   children: [
        //     const SizedBox(height: double.maxFinite),
        //     VideoWidget(
        //       url: 'https://media.w3.org/2010/05/sintel/trailer.mp4',
        //       fill: fill,
        //       // autoPlay: false,
        //     ),
        //     Positioned(
        //       bottom: 16,
        //       right: 16,
        //       child: FloatingActionButton(
        //         onPressed: () {
        //           setState(() {
        //             fill = !fill;
        //           });
        //         },
        //         child: const Icon(Icons.fullscreen),
        //       ),
        //     )
        //   ],
        // ),
      ),
    );
  }
}
