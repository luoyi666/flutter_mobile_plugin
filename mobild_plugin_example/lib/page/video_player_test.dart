import 'package:flutter/material.dart';

import '../global.dart';

class VideoPlayerTestPage extends StatefulWidget {
  const VideoPlayerTestPage(this.title, {super.key});

  final String title;

  @override
  State<VideoPlayerTestPage> createState() => _VideoPlayerTestPageState();
}

class _VideoPlayerTestPageState extends State<VideoPlayerTestPage> {
  bool fill = false;
  final overlayController = OverlayPortalController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 0,
        backgroundColor: Colors.black,
      ),
      body: const SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              VideoWidget(
                url: 'https://img.tukuppt.com/video_show/2414777/00/01/43/5b4303f2bd257.mp4',
                // fill: fill,
                // color: Colors.green,
              ),
              SizedBox(height: 8),
            ],
          ),
        ),
      ),
      // floatingActionButton: FloatingActionButton(
      //   onPressed: () {
      //     setState(() {
      //       fill = !fill;
      //     });
      //   },
      //   child: const Icon(Icons.fullscreen),
      // ),
    );
  }
}
