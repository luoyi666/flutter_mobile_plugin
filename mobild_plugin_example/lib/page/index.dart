import 'package:flutter/cupertino.dart';

import '../global.dart';
import 'click_test.dart';
import 'demo_test.dart';
import 'vibrate_test.dart';
import 'video_player_test.dart';
import 'video_test.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    List<PageNavModel> listData = [
      PageNavModel('Demo测试', const DemoTestPage('Demo测试')),
      PageNavModel('点击穿透测试', const ClickTestPage('点击穿透测试')),
      PageNavModel('Ignore点击事件测试', const IgnoreListTestPage()),
      PageNavModel('震动测试', const VibrateTestPage('震动测试')),
      PageNavModel('Video视频测试', const VideoTestPage('Video视频测试')),
      PageNavModel('Video视频播放器测试', const VideoPlayerTestPage('Video视频播放器测试')),
      // NavPageModel('富文本编辑器测试', EditorTestPage('富文本编辑器测试')),
    ];
    return CupertinoPageScaffold(
      child: CustomScrollView(
        slivers: [
          const CupertinoSliverNavigationBar(largeTitle: Text('插件测试')),
          SliverList.builder(
            itemCount: listData.length,
            itemBuilder: (context, index) => MyCupertinoListTile(
              title: listData[index].title,
              page: listData[index].page,
            ),
          ),
        ],
      ),
    );
  }
}
