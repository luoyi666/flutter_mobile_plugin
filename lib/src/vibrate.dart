part of flutter_mobile_plugin;

bool? _hasVibrator;

const String _tip = 'VibrateUtil工具类提示';

class VibrateUtil {
  VibrateUtil._();

  static Future<void> _check(bool showLog) async {
    if (!PlatformUtil.isMobileClient) {
      if (showLog) d('仅限手机客户端平台才可以触发震动', _tip);
      return;
    }
    _hasVibrator ??= await Vibration.hasVibrator();
    if (_hasVibrator == false) {
      if (showLog) d('当前设备没有震动器', _tip);
      return;
    }
  }

  /// 触发一次震动
  /// * duration 震动时长
  /// * amplitude 震动强度
  static Future<void> createVibrate({
    int duration = 250,
    int amplitude = 128,
    bool showLog = true,
  }) async {
    _check(showLog);
    Vibration.vibrate(duration: duration, amplitude: amplitude);
  }

  /// 触发双频震动
  /// * duration 震动时长
  /// * amplitude 震动强度
  static Future<void> createDoubleVibrate({
    int duration = 250,
    int amplitude = 128,
    bool showLog = true,
  }) async {
    _check(showLog);
    Vibration.vibrate(duration: duration, amplitude: amplitude);
    await 0.35.delay();
    Vibration.vibrate(duration: duration - 50, amplitude: amplitude);
  }
}
