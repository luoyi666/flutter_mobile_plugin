// part of flutter_mobile_plugin;
//
// class EditorWidget extends StatefulWidget {
//   const EditorWidget({super.key});
//
//   @override
//   State<EditorWidget> createState() => _EditorWidgetState();
// }
//
// class _EditorWidgetState extends State<EditorWidget> {
//   @override
//   Widget build(BuildContext context) {
//     return Column(
//       children: [
//         Expanded(
//           child: WebviewWidget(
//             url: 'http://192.168.2.131:5173/',
//             onCreated: (controller) async {
//               if (kIsWeb) {
//                 var webMessageChannel = await controller.createWebMessageChannel();
//                 var port1 = webMessageChannel!.port1;
//               } else {
//                 controller.addJavaScriptHandler(
//                   handlerName: 'myHandlerName',
//                   callback: (args) {
//                     // print arguments coming from the JavaScript side!
//                     logger(args);
//
//                     // return data to the JavaScript side!
//                     return {'bar': 'bar_value', 'baz': 'baz_value'};
//                   },
//                 );
//               }
//             },
//           ),
//         ),
//         Container(
//           height: 50,
//           padding: const EdgeInsets.symmetric(horizontal: 16),
//           decoration: const BoxDecoration(
//             border: Border(
//               top: BorderSide(
//                 color: Colors.grey,
//                 width: 0.5,
//               ),
//             ),
//           ),
//           child: Row(
//             children: [
//               IconButton(
//                 onPressed: () {},
//                 icon: Icon(
//                   Icons.image,
//                   color: Colors.grey.shade800,
//                 ),
//               ),
//               IconButton(
//                 onPressed: () {},
//                 icon: Icon(
//                   Icons.video_camera_back,
//                   color: Colors.grey.shade800,
//                 ),
//               ),
//               IconButton(
//                 onPressed: () {},
//                 icon: Icon(
//                   Icons.video_camera_back,
//                   color: Colors.grey.shade800,
//                 ),
//               ),
//             ],
//           ),
//         ),
//       ],
//     );
//   }
// }
