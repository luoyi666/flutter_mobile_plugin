part of flutter_mobile_plugin;

/// 视频加载状态
enum _VideoLoadStatus {
  /// 待加载
  pending,

  /// 加载完成
  success,

  /// 加载失败
  fail,
}

/// 视频播放状态
enum _VideoPlayStatus {
  /// 播放中
  play,

  /// 暂停中
  pause,

  /// 播放完成
  completed,
}

class _VideoController extends GetxController {
  _VideoController(
    this.id,
    this.context, {
    this.color,
  });

  final String id;
  final BuildContext context;
  final Color? color;

  /// video_player视频控制器
  late VideoPlayerController videoController;

  /// 视频加载状态
  final videoLoadStatus = _VideoLoadStatus.pending.obs;

  /// 视频播放状态
  final videoPlayStatus = _VideoPlayStatus.pause.obs;

  /// 视频处于缓冲加载中
  final isBuffering = false.obs;

  /// 是否显示视频控制器
  final showControl = false.obs;

  /// 用户是否进入全屏播放界面
  final isFullScreen = false.obs;

  /// 当用户左右滑动屏幕、拖拽进度条时在视频中间显示拖拽时间
  final showDragTime = false.obs;

  /// 是否以倍速播放
  final showDoubleSpeedPlay = false.obs;

  /// 视频当前播放的进度
  final videoPosition = 0.obs;

  final videoBuffer = 0.obs;

  /// 用户手动拖拽视频进度，-1表示用户还未拖拽
  final dragProgress = (-1.0).obs;

  /// 是否隐藏视频中心播放按钮，如果autoPlay为true，则它将自动为true
  final hideCenterPlayButton = false.obs;

  /// 自动隐藏视频控制器事件
  Timer? autoHideControlTimer;

  /// 显示缓冲窗口控制器事件，如果视频缓冲时间超过2秒，则显示缓冲加载提示
  Timer? showBufferPanelTimer;

  /// 播放、暂停图标
  IconData get playIcon {
    switch (videoPlayStatus.value) {
      case _VideoPlayStatus.play:
        return Icons.pause;
      case _VideoPlayStatus.pause:
        return Icons.play_arrow;
      case _VideoPlayStatus.completed:
        return Icons.refresh;
    }
  }

  /// 视频总时长
  int get videoDuration => DartUtil.safeInt(videoController.value.duration.inMilliseconds);

  /// 视频播放的进度百分比
  double get videoProgress => videoDuration > 0 ? double.parse((videoPosition.value / videoDuration).toStringAsFixed(2)) * 100 : 0;

  /// 视频缓冲的进度百分比
  double get videoBufferProgress => videoDuration > 0 ? double.parse((videoBuffer.value / videoDuration).toStringAsFixed(2)) * 100 : 0;

  /// 视频总时长文字
  String get videoDurationText => formatDuration(videoDuration);

  /// 视频播放的当前进度文字
  String get videoPositionText => formatDuration(videoPosition.value);

  /// 拖拽的进度文字
  String get dragPositionText => formatDuration(getDragPosition(dragProgress.value));

  Timer? _getBufferTimer;

  /// 切换显示、隐藏控制器
  void toggleControl() {
    showControl.value = !showControl.value;
    // 2秒后自动隐藏
    if (showControl.value) {
      startHideControlTimer();
    }
  }

  /// 切换播放、暂停
  void togglePause() async {
    videoController.value.isPlaying ? await videoController.pause() : await videoController.play();
    videoPlayStatus.value = videoController.value.isPlaying ? _VideoPlayStatus.play : _VideoPlayStatus.pause;
    startHideControlTimer();
  }

  /// 启动自动隐藏控制器计时器
  void startHideControlTimer() {
    if (autoHideControlTimer != null) autoHideControlTimer!.cancel();
    autoHideControlTimer = Timer(const Duration(seconds: 5), () {
      showControl.value = false;
    });
  }

  /// 启动显示loading计时器，如果视频在2秒内未加载，则显示loading
  void startShowBufferPanelTimer() {
    if (showBufferPanelTimer != null) showBufferPanelTimer!.cancel();
    showBufferPanelTimer = Timer(const Duration(seconds: 2), () {
      isBuffering.value = videoController.value.isBuffering;
    });
  }

  /// 进入全屏播放
  Future<void> toFullScreen() async {
    // 预先进入全屏以及固定手机屏幕方向
    StatusBarControl.setFullscreen(true);
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.landscapeRight,
    ]);
    isFullScreen.value = true;
    await Navigator.push(
      context,
      PageRouteBuilder(
        transitionDuration: Duration.zero,
        pageBuilder: (BuildContext context, Animation animation, Animation secondaryAnimation) {
          return _FullVideoWidget(id);
        },
      ),
    );
    isFullScreen.value = false;
    // 退出全屏播放页面后重置设备状态
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    StatusBarControl.setFullscreen(false);
    showControl.value = true;
    startHideControlTimer();
  }

  /// 更新拖拽进度条，传入的value为视频进度百分比
  void updateDragProgress(double value) {
    if (value < 0) value = 0;
    if (value > 100) value = 100;
    dragProgress.value = value;
  }

  /// 设置视频进度
  void setVideoProgress(double value) async {
    if (value < 0) value = 0;
    if (value > 100) value = 100;

    await videoController.seekTo(Duration(milliseconds: getDragPosition(value)));
    dragProgress.value = -1;
  }

  /// 根据百分比获取视频时间
  int getDragPosition(double value) {
    return (videoController.value.duration.inMilliseconds / 100 * value).toInt();
  }

  /// 开启播放
  Future<void> enablePlay() async {
    await videoController.initialize();
    videoLoadStatus.value = _VideoLoadStatus.success;
    videoController.play();
    // 每秒获取当前视频的缓存范围
    _getBufferTimer = Timer.periodic(const Duration(seconds: 1), (timer) {
      if (videoController.value.isBuffering) {
        videoBuffer.value = videoController.value.buffered[0].end.inMilliseconds;
      }
    });
  }

  /// 格式化播放时间
  String formatDuration(int ms) {
    int seconds = ms ~/ 1000;
    final int hours = seconds ~/ 3600;
    seconds = seconds % 3600;
    final minutes = seconds ~/ 60;
    seconds = seconds % 60;

    final hoursString = hours >= 10
        ? '$hours'
        : hours == 0
            ? '00'
            : '0$hours';

    final minutesString = minutes >= 10
        ? '$minutes'
        : minutes == 0
            ? '00'
            : '0$minutes';

    final secondsString = seconds >= 10
        ? '$seconds'
        : seconds == 0
            ? '00'
            : '0$seconds';

    final formattedTime = '${hoursString == '00' ? '' : '$hoursString:'}$minutesString:$secondsString';

    return formattedTime;
  }

  /// 销毁控制器
  void destoryController() {
    videoController.dispose();
    _getBufferTimer?.cancel();
  }

  @override
  void onInit() {
    super.onInit();
    ever(videoPosition, (callback) {
      if (videoPlayStatus.value == _VideoPlayStatus.completed) {
        if (videoProgress < 100) {
          videoPlayStatus.value = _VideoPlayStatus.play;
          videoController.play();
        }
      }
    });
  }

  @override
  void onClose() {
    destoryController();
    super.onClose();
  }
}

class VideoWidget extends StatefulWidget {
  const VideoWidget({
    super.key,
    this.url,
    this.asset,
    this.file,
    this.autoPlay = true,
    this.looping = false,
    this.fill = false,
    this.aspectRatio = 16 / 9,
    this.color,
  });

  /// 从网络地址加载
  final String? url;

  /// 从assets静态目录加载
  final String? asset;

  /// 从本地文件加载
  final String? file;

  /// 是否自动播放视频，默认true
  final bool autoPlay;

  /// 是否循环播放，默认false
  final bool looping;

  /// 视频是否充满整个屏幕，默认false，若为true参数[aspectRatio]将失效
  final bool fill;

  /// 播放视频的容器宽高比(是包裹视频的[Container]容器，并非视频本身)，默认为传统的16/9
  final double aspectRatio;

  /// 自定义视频控件颜色，默认当前主题色
  final Color? color;

  @override
  State<VideoWidget> createState() => VideoWidgetState();
}

class VideoWidgetState<T extends VideoWidget> extends State<T> {
  late String id;
  late _VideoController _controller;

  @override
  void initState() {
    super.initState();
    id = uuidStr;
    _controller = Get.put(_VideoController(id, context, color: widget.color), tag: id);
    init();
  }

  @override
  void deactivate() {
    super.deactivate();
    Get.delete<_VideoController>(tag: id);
  }

  Future<void> init() async {
    VideoPlayerOptions options = VideoPlayerOptions(
      mixWithOthers: false,
    );
    if (!DartUtil.isEmpty(widget.url)) {
      _controller.videoController = VideoPlayerController.networkUrl(
        Uri.parse(widget.url!),
        videoPlayerOptions: options,
      );
    } else if (!DartUtil.isEmpty(widget.asset)) {
      _controller.videoController = VideoPlayerController.asset(
        widget.asset!,
        videoPlayerOptions: options,
      );
    } else if (!DartUtil.isEmpty(widget.file)) {
      _controller.videoController = VideoPlayerController.file(
        File(widget.file!),
        videoPlayerOptions: options,
      );
    } else {
      throw '视频加载异常';
    }
    if (widget.autoPlay) _controller.hideCenterPlayButton.value = true;
    _controller.videoController.setLooping(widget.looping);
    _controller.videoController.addListener(() {
      _controller.videoPosition.value = _controller.videoController.value.position.inMilliseconds;
      if (_controller.videoController.value.hasError) {
        _controller.videoLoadStatus.value = _VideoLoadStatus.fail;
      }
      if (_controller.videoController.value.isCompleted) {
        _controller.videoPlayStatus.value = _VideoPlayStatus.completed;
      }
      _controller.startShowBufferPanelTimer();
    });

    if (widget.autoPlay) {
      _controller.enablePlay();
      _controller.videoPlayStatus.value = _VideoPlayStatus.play;
    }
  }

  @override
  Widget build(BuildContext context) {
    return widget.fill
        ? _videoContainerWidget
        : AspectRatio(
            aspectRatio: widget.aspectRatio,
            child: _videoContainerWidget,
          );
  }

  /// 视频容器
  Widget get _videoContainerWidget => Obx(
        () => Stack(
          children: [
            _VideoEventWidget(
              id: id,
              child: Material(
                color: Colors.black,
                child: Center(
                  child: _controller.videoLoadStatus.value == _VideoLoadStatus.pending
                      ? videoLoadingWidget
                      : _controller.videoLoadStatus.value == _VideoLoadStatus.success
                          ? videoWidget
                          : videoLoadFailWidget,
                ),
              ),
            ),
            _controller.hideCenterPlayButton.value
                ? Positioned(
                    bottom: 0,
                    left: 0,
                    right: 0,
                    child: _VideoControlContainer(
                      id: id,
                      height: 44,
                      begin: Alignment.bottomCenter,
                      end: Alignment.topCenter,
                      child: Row(
                        children: [
                          _VideoPlayButton(id),
                          Expanded(child: _VideoSlider(id)),
                          GestureDetector(
                            onTap: _controller.toFullScreen,
                            child: const Icon(
                              Icons.fullscreen,
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                : _StartPlayVideoButton(id),
            _VideoProgressTextWidget(id),
            _VideoDoubleSpeedTextWidget(id),
            if (_controller.isBuffering.value) IgnorePointer(child: Center(child: videoLoadingWidget)),
          ],
        ),
      );

  /// 视频加载中
  Widget get videoLoadingWidget => buildCupertinoLoading(radius: 12, color: Colors.white);

  /// 视频加载成功后显示视频组件
  Widget get videoWidget => AspectRatio(
        aspectRatio: _controller.videoController.value.aspectRatio,
        child: IgnorePointer(child: VideoPlayer(_controller.videoController)),
      );

  /// 视频加载失败
  Widget get videoLoadFailWidget => Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            '加载失败',
            style: TextStyle(color: Colors.grey.shade300),
          ),
          const SizedBox(height: 16),
          SizedBox(
            height: 36,
            child: FilledButton(
              onPressed: () async {
                _controller.destoryController();
                _controller.videoLoadStatus.value = _VideoLoadStatus.pending;
                await init();
              },
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(widget.color),
              ),
              child: const Text('重新加载'),
            ),
          )
        ],
      );
}

/// 视频手势事件
class _VideoEventWidget extends StatelessWidget {
  const _VideoEventWidget({
    required this.id,
    required this.child,
  });

  final String id;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    _VideoController controller = Get.find(tag: id);
    return Obx(() {
      bool hideCenterPlayButton = controller.hideCenterPlayButton.value;
      bool videoLoadSuccess = controller.videoLoadStatus.value == _VideoLoadStatus.success;
      bool videoIsPlay = controller.videoPlayStatus.value == _VideoPlayStatus.play;
      bool allowDrag = GetPlatform.isMobile && hideCenterPlayButton && videoLoadSuccess;
      bool allowOnLong = GetPlatform.isMobile && hideCenterPlayButton && videoLoadSuccess && videoIsPlay;
      return GestureDetector(
        // behavior: HitTestBehavior.translucent,
        onTap: hideCenterPlayButton
            ? () {
                controller.toggleControl();
              }
            : null,
        onDoubleTap: hideCenterPlayButton && videoLoadSuccess
            ? () {
                HapticFeedback.heavyImpact();
                controller.togglePause();
                controller.startHideControlTimer();
              }
            : null,
        // 水平滑动控制播放进度
        onHorizontalDragStart: allowDrag
            ? (e) {
                controller.dragProgress.value = controller.videoProgress;
              }
            : null,
        onHorizontalDragUpdate: allowDrag
            ? (e) {
                controller.showDragTime.value = true;
                if (controller.showControl.value) controller.startHideControlTimer();
                controller.updateDragProgress(controller.dragProgress.value + e.delta.dx / 4);
              }
            : null,
        onHorizontalDragEnd: allowDrag
            ? (e) {
                controller.showDragTime.value = false;
                controller.setVideoProgress(controller.dragProgress.value);
              }
            : null,
        onLongPress: allowOnLong
            ? () {
                HapticFeedback.heavyImpact();
                controller.showDoubleSpeedPlay.value = true;
                controller.videoController.setPlaybackSpeed(2);
              }
            : null,
        onLongPressUp: allowOnLong
            ? () {
                controller.showDoubleSpeedPlay.value = false;
                controller.videoController.setPlaybackSpeed(1);
              }
            : null,
        child: child,
      );
    });
  }
}

class _VideoControlContainer extends StatelessWidget {
  const _VideoControlContainer({
    required this.id,
    required this.child,
    this.height,
    this.begin = Alignment.topCenter,
    this.end = Alignment.bottomCenter,
  });

  final String id;
  final Widget child;
  final double? height;

  /// 控制器黑色遮罩渐变方向
  final AlignmentGeometry begin;
  final AlignmentGeometry end;

  @override
  Widget build(BuildContext context) {
    late final _VideoController controller = Get.find(tag: id);
    return Obx(
      () => AbsorbPointer(
        absorbing: !controller.showControl.value,
        child: AnimatedOpacity(
          opacity: controller.showControl.value ? 1 : 0,
          duration: const Duration(milliseconds: 150),
          child: Container(
            height: height,
            padding: const EdgeInsets.symmetric(horizontal: 8),
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: const [
                  Color.fromRGBO(0, 0, 0, 0.8),
                  Color.fromRGBO(0, 0, 0, 0.7),
                  Color.fromRGBO(0, 0, 0, 0.6),
                  Color.fromRGBO(0, 0, 0, 0.5),
                  Color.fromRGBO(0, 0, 0, 0.4),
                  Color.fromRGBO(0, 0, 0, 0.3),
                  Color.fromRGBO(0, 0, 0, 0.2),
                  Color.fromRGBO(0, 0, 0, 0.1),
                  Colors.transparent
                ],
                begin: begin,
                end: end,
              ),
            ),
            child: child,
          ),
        ),
      ),
    );
  }
}

class _VideoProgressTextWidget extends StatelessWidget {
  const _VideoProgressTextWidget(this.id);

  final String id;

  @override
  Widget build(BuildContext context) {
    final _VideoController controller = Get.find(tag: id);
    return IgnorePointer(
      child: Center(
        child: Obx(
          () => AnimatedOpacity(
            opacity: controller.showDragTime.value ? 1 : 0,
            duration: const Duration(milliseconds: 150),
            child: Container(
              padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 12),
              decoration: BoxDecoration(
                color: Colors.black87,
                borderRadius: BorderRadius.circular(6),
              ),
              child: Obx(
                () => Text(
                  '${controller.dragProgress.value < 0 ? controller.videoPositionText : controller.dragPositionText} / ${controller.videoDurationText}',
                  style: const TextStyle(
                    fontSize: 12,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class _VideoDoubleSpeedTextWidget extends StatelessWidget {
  const _VideoDoubleSpeedTextWidget(this.id);

  final String id;

  @override
  Widget build(BuildContext context) {
    final _VideoController controller = Get.find(tag: id);
    return Positioned(
      left: 0,
      right: 0,
      top: 30,
      child: IgnorePointer(
        child: Center(
          child: Obx(
            () => AnimatedOpacity(
              opacity: controller.showDoubleSpeedPlay.value ? 1 : 0,
              duration: const Duration(milliseconds: 150),
              child: Container(
                  padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 12),
                  decoration: BoxDecoration(
                    color: Colors.black87,
                    borderRadius: BorderRadius.circular(6),
                  ),
                  child: const Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      _VideoFastForwardWidget(
                        width: 12,
                        height: 18,
                        color: Colors.grey,
                      ),
                      SizedBox(width: 2),
                      Text(
                        '倍速播放中',
                        style: TextStyle(
                          fontSize: 12,
                          color: Colors.white,
                        ),
                      ),
                    ],
                  )),
            ),
          ),
        ),
      ),
    );
  }
}

/// autoPlay为false时，视频中间的播放按钮
class _StartPlayVideoButton extends StatefulWidget {
  const _StartPlayVideoButton(this.id);

  final String id;

  @override
  State<_StartPlayVideoButton> createState() => _StartPlayVideoButtonState();
}

class _StartPlayVideoButtonState extends State<_StartPlayVideoButton> {
  late final _VideoController _controller = Get.find(tag: widget.id);
  bool isPlay = false;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: isPlay
          ? null
          : () async {
              setState(() {
                isPlay = true;
              });
              await _controller.enablePlay();
              AsyncUtil.delayed(() {
                _controller.hideCenterPlayButton.value = true;
                _controller.videoPlayStatus.value = _VideoPlayStatus.play;
              }, 300);
            },
      child: Center(
        child: UnconstrainedBox(
          child: AnimatedOpacity(
            opacity: isPlay ? 0.0 : 1.0,
            duration: const Duration(milliseconds: 300),
            child: Container(
              padding: const EdgeInsets.all(12),
              decoration: BoxDecoration(
                color: Colors.grey.shade700,
                shape: BoxShape.circle,
              ),
              child: Center(
                child: Icon(
                  isPlay ? Icons.pause : Icons.play_arrow,
                  size: 32,
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class _VideoPlayButton extends StatelessWidget {
  const _VideoPlayButton(this.id);

  final String id;

  @override
  Widget build(BuildContext context) {
    _VideoController controller = Get.find(tag: id);
    return GestureDetector(
      onTap: () {
        HapticFeedback.heavyImpact();
        controller.togglePause();
      },
      child: Obx(
        () => Icon(
          controller.playIcon,
          color: Colors.white,
        ),
      ),
    );
  }
}

/// 构建视频进度栏
class _VideoSlider extends StatefulWidget {
  const _VideoSlider(this.id);

  final String id;

  @override
  State<_VideoSlider> createState() => _VideoSliderState();
}

class _VideoSliderState extends State<_VideoSlider> {
  late final _VideoController _controller = Get.find(tag: widget.id);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        const SizedBox(width: 12),
        Expanded(
          child: SliderTheme(
            data: SliderThemeData(
              trackHeight: 3,
              inactiveTrackColor: Colors.grey.withOpacity(0.5),
              activeTrackColor: _controller.color,
              activeTickMarkColor: _controller.color,
              secondaryActiveTrackColor: Colors.grey,
              thumbColor: _controller.color,
              thumbShape: const RoundSliderThumbShape(enabledThumbRadius: 6),
              overlayShape: const RoundSliderOverlayShape(overlayRadius: 12),
              trackShape: _CustomVideoTrackShape(addHeight: 1),
            ),
            child: Obx(
              () => Slider(
                min: 0,
                max: 100,
                // 用户若手动拖拽，则更新手动拖拽的进度值，否则使用视频播放的进度值
                value: _controller.dragProgress.value < 0 ? _controller.videoProgress : _controller.dragProgress.value,
                secondaryTrackValue: _controller.videoBufferProgress,
                onChanged: (double value) {
                  var $value = value.truncate();
                  if ($value != _controller.dragProgress.value) {
                    _controller.showDragTime.value = true;
                    _controller.dragProgress.value = $value.toDouble();
                  }
                  _controller.startHideControlTimer();
                },
                onChangeEnd: (double value) {
                  _controller.showDragTime.value = false;
                  _controller.setVideoProgress(value);
                },
              ),
            ),
          ),
        ),
        const SizedBox(width: 12),
        Obx(
          () => SizedBox(
            width: 80,
            child: Text(
              '${_controller.videoPositionText}/${_controller.videoDurationText}',
              style: const TextStyle(
                fontSize: 10,
                color: Colors.white,
              ),
            ),
          ),
        ),
      ],
    );
  }
}

/// 自定义视频滑块样式
class _CustomVideoTrackShape extends RoundedRectSliderTrackShape {
  _CustomVideoTrackShape({this.addHeight = 0});

  //增加选中滑块的高度,系统默认+2·
  double addHeight;

  ///去掉默认边距
  @override
  Rect getPreferredRect({
    required RenderBox parentBox,
    Offset offset = Offset.zero,
    required SliderThemeData sliderTheme,
    bool isEnabled = false,
    bool isDiscrete = false,
  }) {
    final double trackHeight = sliderTheme.trackHeight ?? 1;
    final double trackLeft = offset.dx;
    final double trackTop = offset.dy + (parentBox.size.height - trackHeight) / 2;
    final double trackWidth = parentBox.size.width;
    return Rect.fromLTWH(trackLeft, trackTop, trackWidth, trackHeight);
  }

  @override
  void paint(
    PaintingContext context,
    Offset offset, {
    required RenderBox parentBox,
    required SliderThemeData sliderTheme,
    required Animation<double> enableAnimation,
    required TextDirection textDirection,
    required Offset thumbCenter,
    Offset? secondaryOffset,
    bool isDiscrete = false,
    bool isEnabled = false,
    double additionalActiveTrackHeight = 2,
  }) {
    super.paint(context, offset,
        parentBox: parentBox,
        sliderTheme: sliderTheme,
        enableAnimation: enableAnimation,
        textDirection: textDirection,
        thumbCenter: thumbCenter,
        secondaryOffset: secondaryOffset,
        isDiscrete: isDiscrete,
        isEnabled: isEnabled,
        additionalActiveTrackHeight: addHeight);
  }
}

/// 视频快进图标
class _VideoFastForwardWidget extends StatefulWidget {
  const _VideoFastForwardWidget({
    this.width = 14,
    this.height = 20,
    this.color = Colors.grey,
  });

  final double width;
  final double height;
  final Color color;

  @override
  State<_VideoFastForwardWidget> createState() => _VideoFastForwardWidgetState();
}

class _VideoFastForwardWidgetState extends State<_VideoFastForwardWidget> {
  late List<bool> _list;
  static const int _time = 400;
  int count = 3;
  Timer? _timer;

  void _init([bool flag = false]) {
    _list = List.generate(count, (index) => false).toList();
    _timer?.cancel();
    _timer = Timer.periodic(const Duration(milliseconds: _time - 100), (timer) {
      int index = _list.indexOf(true);
      if (index == -1) {
        _list[0] = true;
      } else {
        _list[index] = false;
        if (index == _list.length - 1) {
          index = 0;
        } else {
          index++;
        }
        _list[index] = true;
      }
      if (mounted) setState(() {});
    });
  }

  @override
  void initState() {
    super.initState();
    _init();
  }

  @override
  void didUpdateWidget(covariant _VideoFastForwardWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
    _init(true);
  }

  @override
  void dispose() {
    _timer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: List.generate(
        count,
        (index) => Padding(
          padding: const EdgeInsets.only(left: 4),
          child: ClipPath(
              clipper: _VideoClipper(4),
              child: AnimatedOpacity(
                opacity: _list[index] ? 1 : 0.4,
                duration: const Duration(milliseconds: _time),
                curve: Curves.easeOut,
                child: Container(
                  width: widget.width,
                  height: widget.height,
                  color: widget.color,
                ),
              )),
        ),
      ).toList(),
    );
  }
}

class _VideoClipper extends CustomClipper<Path> {
  final double radius;

  _VideoClipper(this.radius);

  @override
  Path getClip(Size size) {
    var path = Path();
    path.moveTo(0, 0);
    path.lineTo(0, size.height - radius);
    path.quadraticBezierTo(radius / 4, size.height - radius / 2, radius, size.height - radius);
    path.lineTo(radius, size.height - radius);
    path.lineTo(size.width - radius, size.height / 2 + radius / 2);
    path.quadraticBezierTo(size.width - radius / 4, size.height / 2, size.width - radius, size.height / 2 - radius / 2);
    path.lineTo(size.width - radius, size.height / 2 - radius / 2);
    path.lineTo(radius, radius);
    path.quadraticBezierTo(radius / 4, radius / 2, 0, radius);
    path.lineTo(0, radius);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    return true;
  }
}

/// 全屏视频播放页面
class _FullVideoWidget extends StatefulWidget {
  const _FullVideoWidget(this.id);

  final String id;

  @override
  State<_FullVideoWidget> createState() => _FullVideoWidgetState();
}

class _FullVideoWidgetState extends State<_FullVideoWidget> {
  late final _VideoController _controller = Get.find(tag: widget.id);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Stack(
        children: [
          _VideoEventWidget(
            id: widget.id,
            child: Container(
              alignment: Alignment.center,
              color: Colors.black,
              child: AspectRatio(
                aspectRatio: _controller.videoController.value.aspectRatio,
                child: VideoPlayer(_controller.videoController),
              ),
            ),
          ),
          _VideoFullScreenControl(widget.id),
          _VideoProgressTextWidget(widget.id),
          _VideoDoubleSpeedTextWidget(widget.id),
        ],
      ),
    );
  }
}

class _VideoFullScreenControl extends StatelessWidget {
  const _VideoFullScreenControl(this.id);

  final String id;

  @override
  Widget build(BuildContext context) {
    _VideoController controller = Get.find(tag: id);
    return Stack(
      children: [
        Positioned(
          top: 0,
          left: 0,
          right: 0,
          child: _VideoControlContainer(
            id: id,
            height: 56,
            child: Row(
              children: [
                IconButton(
                  onPressed: () {
                    context.pop();
                  },
                  color: Colors.white,
                  icon: const Icon(Icons.arrow_back),
                ),
              ],
            ),
          ),
        ),
        Positioned(
          bottom: 0,
          left: 0,
          right: 0,
          child: _VideoControlContainer(
            id: id,
            height: 56,
            child: Row(
              children: [
                _VideoPlayButton(id),
                Expanded(child: _VideoSlider(id)),
                GestureDetector(
                  onTap: controller.toFullScreen,
                  child: const Icon(
                    Icons.fullscreen,
                    color: Colors.white,
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
