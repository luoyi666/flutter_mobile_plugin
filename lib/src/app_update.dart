part of flutter_mobile_plugin;

/// App更新工具类，注意：这仅仅是前端UI实现，你需要自己实现后端逻辑
class AppUpdateUtil {
  AppUpdateUtil._();

  /// 初始化App更新
  static void init({bool showLog = true}) {
    if (GetPlatform.isAndroid) {
      FlutterXUpdate.init(
        ///是否输出日志
        debug: showLog,

        ///是否使用post请求
        isPost: false,

        ///post请求是否是上传json
        isPostJson: false,

        ///是否开启自动模式
        isWifiOnly: false,

        ///是否开启自动模式
        isAutoMode: false,

        ///需要设置的公共参数
        supportSilentInstall: false,

        ///在下载过程中，如果点击了取消的话，是否弹出切换下载方式的重试提示弹窗
        enableRetry: false,
      ).then(
        (value) {
          if (showLog) i("初始化成功: $value");
        },
      ).catchError((error) {
        if (showLog) e(error);
      });

      FlutterXUpdate.setErrorHandler(
        onUpdateError: (message) async {
          if (showLog) e(message);
        },
      );

      FlutterXUpdate.setUpdateHandler(
        onUpdateError: (message) async {
          if (showLog) i(message);
          //下载失败
          if (message?["code"] == 4000) {
            FlutterXUpdate.showRetryUpdateTipDialog(
                retryContent: 'Github被墙无法继续下载，是否考虑切换蒲公英下载？', retryUrl: 'https://www.pgyer.com/flutter_learn');
          }
        },
      );
    }
  }

  /// 根据模型对象显示更新弹窗
  static void update(UpdateEntity model) {
    FlutterXUpdate.updateByInfo(
      updateEntity: model,
    );
  }

  /// 将接口返回的数据转换成更新实体类
  static UpdateEntity jsonToModel(Map jsonData) {
    int size = (jsonData['fileSize'] as int) ~/ 1024;
    return UpdateEntity(
      hasUpdate: true,
      isIgnorable: false,
      versionCode: jsonData['versionCode'],
      versionName: 'v${jsonData['versionName']}',
      updateContent: (jsonData['updateDesc'] as List).join('\n'),
      downloadUrl: jsonData['downloadUrl'],
      apkSize: size,
    );
  }
}
