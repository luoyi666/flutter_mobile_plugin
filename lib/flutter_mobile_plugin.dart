library flutter_mobile_plugin;

import 'dart:async';
import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:luoyi_flutter_base/luoyi_flutter_base.dart';
import 'package:mini_getx/mini_getx.dart';

import 'package:wechat_camera_picker/wechat_camera_picker.dart';

import 'flutter_mobile_plugin.dart';

import 'package:flutter_xupdate/flutter_xupdate.dart';

import 'package:video_player/video_player.dart';

import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';
import 'package:azlistview/azlistview.dart';
import 'package:lpinyin/lpinyin.dart';
import 'package:wechat_assets_picker/wechat_assets_picker.dart';

export 'package:luoyi_flutter_plugin/luoyi_flutter_plugin.dart';

/// 底部弹窗
export 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

/// 震动插件
export 'package:vibration/vibration.dart';

/// 优化官方TabView
export 'package:extended_tabs/extended_tabs.dart';

/// 打开文件
export 'package:open_file_plus/open_file_plus.dart';

/// 权限处理
export 'package:permission_handler/permission_handler.dart';

/// 控制状态栏插件，注意，设置全屏时android默认保留安全区域，请修改android/app/src/main/res/values/styles.xml中的代码，
/// 往 <style name="NormalTheme" parent="@android:style/Theme.Light.NoTitleBar"><style/> 插入以下标签：
///
/// ```xml
/// <item name="android:windowLayoutInDisplayCutoutMode">shortEdges</item>
/// <item name="android:navigationBarColor">@android:color/transparent</item>
/// ```
export 'package:status_bar_control/status_bar_control.dart';

part 'src/bottom_modal/modal_router.dart';

part 'src/form/form_file_upload.dart';

part 'src/app_update.dart';

part 'src/cascader.dart';

part 'src/file_picker.dart';

part 'src/index_list.dart';

part 'src/permission.dart';

part 'src/vibrate.dart';

part 'src/video.dart';
